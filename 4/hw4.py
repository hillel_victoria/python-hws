# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно.

print('Task 1')

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']

lst2 = [elem for elem in lst1 if type(elem) is str]
print(lst2)

# Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.

print('Task 2')

random_list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
print('Just print: ',[random_elem for random_elem in random_list if random_elem > 21 and random_elem < 74]) # не видаляє, але виводить :)

# delete:

random_list2 = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
for random_elem2 in random_list2.copy():
    if random_elem2 < 21 or random_elem2 > 74:
       random_list2.remove(random_elem2)
print('Deleted: ',random_list2)

# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

print('Task 3')

user_srt = 'hello one two three zero'

elem_count = 0

for elem_str in user_srt.split():
    if elem_str.endswith('o'):
        elem_count += 1
print('Number of words that end with \'o\': ', elem_count)

# user input:
print('Task 3/user input')

elem_count = 0
user_str = input('Type some words (please use a space to split them): ')
if len(user_str) > 1:       # if user_str.isalpha() тут не спрацювало
    for elem_str in user_str.split():
        if elem_str.endswith('o'):
            elem_count += 1
    print('Number of words that end with \'o\': ', elem_count)
else:
    print(f'{user_str} is too short, please try again')
