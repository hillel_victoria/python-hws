# ------------------Ternary operator - тернарний оператор

is_even = None

number = 123
if number % 2 == 0:
    is_even = True
else:
    is_even = False

is_even = True if number % 2 == 0 else False # this is better
# opt1 if cond else opt2
# print(is_even)

#------------------- List
    # --------syntax

my_list = [1,'2', True, None] # can contain any data types; can add/remove data
print(type(my_list))
print(my_list)

my_list1 = list()
print(my_list1)

my_list2 = [3,4,5]
res = my_list + my_list2
print(f'my_list + my_list2   {res}')

res = res * 2
print(f'res * 2   {res}')

    # --------methods

# my_list = []
# print(my_list)
#
# my_list += [1]
# print(my_list)
#
# my_list += [3]
# print(my_list)

print('------------methods--------')

my_list = []
print(my_list)
my_list.append(1)
print(my_list)
my_list.append('asdfu')
print(my_list)
my_list.append(True)
print(my_list)

print(1 in my_list) # from line 47,49,51
print('asdfu' in my_list)

print(my_list.index('asdfu'))
print(my_list[2]) # indexes same as string
print(my_list[:2])

print(my_list[1].upper()) # because 1st elements 'asdfu' is String we can use String actions

#------------------- None type

my_none = None

my_list = [1, 2.3, True, None, '0123456789', [1,2,3,4]] # list can be element from list

print(my_list[4][-1]) # print last symbol [-1] of 4th element '0123456789'
print(type(my_list[4]))
print(my_list[5][3])

my_list[5].append(5)

print(my_list)
print(my_list[5].append([]))

my_list.pop(3) # remove 3rd element if exist
print(my_list)

my_list.remove('0123456789') # remove this element
print(my_list)

my_list[0] = ['a','b','c'] # remove 0th element according to index
print(my_list)





    # --------iteration (while)
print('--------iteration (while)-----------')

idx = 0

my_list = [1, 2.3, True, None, '0123456789', [1,2,3,4], '0123456789']
while idx < len(my_list):
    print('->', my_list[idx], type(my_list[idx]))
    idx += 1

idx = 0
my_str = 'asdfghjkl;'

while idx < len(my_str):
    print('->', my_str[idx], type(my_str[idx]))
    idx += 1

    # --------iteration (for)
print('--------Loop for--------')

# ------------------- Loop for

# the cycle won't be endless and we won't miss any element

my_list = [1, 2.3, True, None, '0123456789', 'abc', [1,2,3,4], '0123456789']

for elem in my_list: # i - it's an example, we can write 'element' for example, better to use a name, not letter
    print('->', elem, type(elem))
    # if elem is True:
    #     break
    if elem == 'abc':
        break

for elem in my_list:
    if elem == 'abc':
        continue
    print('->', elem, type(elem)) # 'abc' won't be typed, even if we have it inn a few times

    if type(elem) == list:
        for inner_element in elem:
            print('inner list element', inner_element)
    # if elem is True:
    #     break

# NEVER CHANGE/ADD ELEMENTS DURING FOR/WHILE, not a good practice:

# for elem in my_list:
#     my_list.append(1)
#     print('Hello')

# example, but not reccomented to keep it in list, registration form
user_data = []

name = 'Vicky'
user_data.append(name)
password = '123'
user_data.append(password)
age = 28
user_data.append(age)

print(user_data)

user_pwd = user_data[1]


# example list:

lst = ['a', 'b', 'c', 'd']
print(lst[0])
print([1])
print('lst index', lst.index('a'))
print(str(lst))
print(bool(lst)) # if there is at least one elem - True, if list is empty - False

if lst:
    pass


# ------------------- mutable\immutable objects
print(' ------------------- mutable\immutable objects -------------')

# immutable
print('----immutable-----')
a = 10
print(id(a))
a += 1
print(id(a))

# mutable
print('----mutable-----')
lst = [1,2,3]
print(id(lst))

lst.append(4)
print(id(lst))

lst1 = [1,2,3]

lst2 = lst1
lst2.append(4)

# print(lst1)
# print(lst2)

lst1 = [1,2,3]

lst2 = lst1.copy()
lst2.append(4)

# print(lst1)
# print(lst2)

lst1 = [1,2, ['a', 'b']]

lst2 = lst1.copy()
lst2.append(4)
lst2[2].append('c') # add to 2nd element

print(lst1)
print(lst2)

import copy

print('---deep copy---')
lst1 = [1,2, ['a', 'b']]
lst2 = copy.deepcopy(lst1)
lst2.append(4)
lst2[2].append('c')

print(id(lst1[2]))
print(id(lst2[2]))
print(lst1)
print(lst2)

# IS operator
print('operator \'is\' ')

# IS - always for true/false, none


