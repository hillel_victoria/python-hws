# Напишіть программу "Касир в кінотеватрі", яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік (можно використати константу або input()).
# - якщо користувачу менше 7 - вивести повідомлення"Де твої батьки?"
# - якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
# - якщо користувачу більше 65 - вивести повідомлення"Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
# - у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"
# P.S. На екран має бути виведено лише одне повідомлення! Макож подумайте над варіантами, коли введені невірні дані

userinput = input('How old are you?:')

if userinput.isdigit() and len(userinput) <= 3:
    userage = int(userinput)
    if '7' in str(userage) and userage < 121:
        print('I\'m Feeling Lucky 777')
    else:
        if userage == 0:
            print('You\'ve just been born, try again later')
        elif userage < 7:
            print('Where are your parents kid?')
        elif userage < 16:
            print('It\'s 18+ movie!')
        elif userage > 120:
            print('Are you zombie? Enter your real age!')
        elif userage > 65:
            print('Show your pension card')
        else:
            print('Sorry, no tickets left!')
else:
    print('Amm... your input is not an age')

