# lesson 2
my_float = 1.9
res = int(my_float)
print(res)
print(type(res))


res = bool (-10)
print(res)
print(type(res))

# str

my_str = 'qwerty'
print(my_str)
print(type(my_str))

my_str = '''qwerty3
dsfsd
dsfsdf
dsfsd'''
print(my_str)
print(type(my_str))

# if we need ' symbol
my_str = "fdgfdg" # not good option, but works
my_str = 'vdvf\'sderfse' #better option with \

print(my_str)
print(type(my_str))

my_str = 'dsfsdf \n \t dfdsf' # new line \n, etc
print(my_str)
print(type(my_str))

#transform to string

my_int = 10
result = str(my_int)
print(result)
print(type(result))

result = int('1234')
print(result)
print(type(result))

result = float('123.45')
print(result)
print(type(result))

my_str1 = 'eyrteywr'
my_str2 = '_qwert'
res = my_str1 + my_str2
print(res)

result = my_str1 + str(123)
print(result)

result = my_str1 * 3 # = my_str1 + my_str1 + my_str1
print(result)
print(type(result))

# same result
my_str1 *= 3
print(result)
print(type(result))

#

name = 'Vicky'
age = 28

# Hello, my name is Artem, I\'m 28

# not very good way, but sometimes still used

res = 'Hello, my name is ' + name + ', I\'m ' + str(age)
print(res)
print(type(res))

# old way is with % percentage syntax

res = 'Hello, my name is  %s, I\'m %s' % (name, age)
print(res)
print(type(res))

# better and used is format way

tpl = 'Hello, my name is {}, I\'m {}' # template
result = tpl.format(name, age)
print(result)
print(type(result))

# another way
tpl = 'Hellooo, my name is {name}, I\'m {age}' # template
result = tpl.format(age=age, name=name,) # order doesn't matter (age=22) will work too
print(result)
print(type(result))


# the best and most used new way f-string
# but we can't save this template, so if we need to use different templates - use the previous way
result =  f'Hey, my name is {name}, I\'m {age} {123}'
print(result)
print(type(result))

result = 'Victoria'
result_len = len(result)
print(result_len)

res = 'a' in result # check if letter is present true/false
print(res)

res = 'Victoria' in result # check if value contains this true/false
print(res)

###################################################################################

# if else

condition = False
if condition:
    print(f'Inner code block IF {condition}') # 4 spaces or TAB will make inner code block, then it will be inside 'if'; bool(condition)
else:
    print(f'Inner code block ELSE {condition}')
    print(f'Inner code block ELSE {condition}')

a = 100
b = 20

res = a > b

if a > b: # False
    print('Inner code block IF a > b')
    print('Inner code block IF a > b')
else:
    print('Inner code block IF a <= b')
    print('Inner code block IF a <=> b')

    a = 100
    b = 20

    res = a > b

if res: # False
    print(f'Inner code block IF a > b')
    print(f'Inner code block IF a > b')
else:
    print('Inner code block IF a <= b')
    print('Inner code block IF a <= b')

my_int = 0

if my_int: # bool(my_int) True
    print('Inner code block IF')
else:
    print('my_int - Inner code block ELSE')


my_str = ''

if my_str: # bool(my_str) True
    print(' my_str - Inner code block IF')
else:
    print('my_str - Inner code block ELSE')

my_str = 'abc'

if my_str == 'abc': # bool(my_str) True
    print('my_str abc - Inner code block IF')
else:
    print('my_str abc - Inner code block ELSE')


my_str = 'abc2'

if 'a' in my_str: # bool(my_str) True
    print('my_str a in - Inner code block IF')
else:
    print('my_str a in - Inner code block ELSE')

my_condition = True

if my_condition:
    print('my_condition - Inner code block IF')
else:
    print('my_condition - Inner code block ELSE')



if my_condition == True: # Bad idea!
    print('my_condition - Inner code block IF')
else:
    print('my_condition - Inner code block ELSE')

if my_condition is True: # Good idea (is)   Check if my_condition is boolean
    print('my_condition - Inner code block IF')
else:
    print('my_condition - Inner code block ELSE')



if type(my_condition) == str: #  Check if my_condition is string (it's boolean)
    print('my_condition is str - Inner code block IF')
else:
    print('my_condition isn\'t str - Inner code block ELSE')


my_condition = True
if type(my_condition) == str: # Check if my_condition is string
    print('my_condition is str - Inner code block IF')
    print('my_condition is str - Inner code block IF')
elif type(my_condition) == bool: # else if (can be many)
    print('my_condition is bool - Inner code block IF')
elif type(my_condition) == int:
    print('my_condition is int - Inner code block IF')
else:
    print('my_condition isn\'t str or bool - Inner code block ELSE')

my_condition = '10;'
if type(my_condition) == str:
    if '1' in my_condition:
        print('11111111') # 4 + 4 spaces or 2 TABS
    print('Inner code block IF STR')
elif type(my_condition) == bool:
    print('Inner code block IF BOOL')


a = 100
b = 20
c = 30

# AND

if a > b and b < c and c > 0: # can be many conditions, all of them should pass
    print('a > b and b < c')

# OR
if a > b or b < c or c <= 10: # can be many conditions, at least one should pass
    print('a > b or b < c')

# AND + OR; AND is executed first
if (a > b or b < c) and (c <= 10 or b > 2): # can be many conditions, at least one should pass; could use () for easy read
    print('condition fulfilled')

# order of executing:
# ()
# **
# * / // %
# + -
# == != < > <= >=
# and
# or
# =

res = (a > b or b < c) and (c <= 10 or b > 2)
print(res)

if (a > b or b < c) and (c <= 10 or b > 2): # can be many conditions, at least one should pass; could use () for easy read
    pass

cond1 = a > b or b < c
cond2 = c <= 10 or b > 2

if cond1 and cond2:
    print('condition fulfilled')

# else and elif are not must in this construction, if 'if' condition is passed - print, if no - skip, same if we have 'if' and 'elif'
print('out from if block')

# string methods

my_str = '1234567823'

res = my_str.replace('23', 'abc')
print(res)

res = my_str.replace('23', '')
print(res)

my_str = '----12345----'
res = my_str.strip('-') # strip = cut
print(res)

res = my_str.lstrip('-') # lstrip = left cut
print(res)

res = my_str.rstrip('-') # rstrip = right cut
print(res)

my_str = 'abcdefGHI'
res = my_str.upper() # change all letters to upper register
print(res)

res = my_str.lower() # change all letters to lower register
print(res)

res = my_str.title() # change to title (firsl letter is big, all other - small)
print(res)

my_str = 'abcabcabc'
res = my_str.count('ab') # number of 'ab' in string
print(res)

my_str = 'abcabcabc'
res = my_str.endswith('bc') # if string is finished with 'bc' true/false
print(res)

my_str = 'abcabcabc'
res = my_str.startswith('bc') # if string is started with 'bc' true/false
print(res)

my_str = '123456'
res = my_str.isdigit() # if string has only digits
print(res)

my_str = 'dfsdf'
res = my_str.isalpha() # if string has only letters
print(res)

my_str = 'dfsd343JHGHJ'
res = my_str.isalnum() # if string has only letters and digits (no spec chars)
print(res)

# user inputs
# are always string (even if there are numbers)! but we can convert after to diff type
# are rarely used, usually when learning, to check different values many times

# print('begin')
# userinput = input('Enter smth:')
#
# if userinput.isdigit():
#     age = int(userinput)
#     print(userinput + ' well done')
# else:
#     print('Wrong input')

# print(f'Your entered str is: {userinput}')
#print(userinput)

userinput = input('How old are you?:')

if userinput.isdigit():
    userage = int(userinput)
    if '7' in str(userage):
        print('I\'m Feeling Lucky')
    else:
        print('Sorry')
else:
    print('Not a number')

