# Find website and write 30 different XPATH locators and 15 CSS locators. Put it in python file i n format:


"""
https://www.saucedemo.com/

XPATH:

1. //input[@class='input_error form_input']
2. //input[@class='submit-button btn_action']
3. //*[@id='root']
4. //*[@id='login_credentials']
5. //div[@id='login_credentials']
6. //*[@id='login_button_container']/div/form
7. //*[@id='root']/div//form
8. //*[@id]
9. //*[@id='login_button_container']/div/form/div[2]
10. //*[@id='login_button_container']//form/div[2]
11. //*[@id='login_button_container']//form/*
12. //h4
13. //*[@id='login_credentials']/h4
14. //style | //div
15. //*[@name='user-name']
16. //*[@id='login_button_container']/ancestor::div
17. //*[@id='login_button_container']/ancestor::div[@class='login_wrapper']
18. //*[@id='login_credentials']/h4/ancestor::div[@id='login_credentials']
19. //*[@id='login_credentials']/h4/ancestor::div
20. //div[@id='login_credentials']/child::*
21. //div[@id='login_credentials']/child::h4
22. //div[@id='login_credentials']/descendant::*
23. //*[@id='login_button_container']/div/form/div[1]/descendant::*
24. //h4[text()='Accepted usernames are:']
25. //*[@id='login_button_container']//form/descendant::div
26. //*[@id='login_button_container']//form/following::*
27. //*[@id=login_button_container]/div/form/div/following-sibling::*
28. //*[@class='login_wrapper']/parent::*
29. //div[@class='login_wrapper']/preceding::*
30. //*[@id='password' and @class='input_error form_input']
31. //*[contains(text(),'user')]
32. //*[contains(@class,'login')]

CSS:

1. div.login_logo
2. div.login_wrapper div.login-box
3. #user-name
4. input.submit-button
5. input.submit-button, input.btn_action
6. div input.input_error
7. div > input
8. div + input
9. div ~ div
10. div ~ div ~ input
11. [data-test]
12. [data-test='password']
13. [class~= btn_action]
14. [class^='input_err']
15. [class$='input']
16. [class*='err']

"""

# Find web site and log in to it using selenium webdriver. It can be Chrome only. But if it works on your PC try different browsers


import time
from selenium.webdriver import Chrome, Safari, Keys
from selenium.webdriver.common.by import By


def test_01():
    chrome_driver = Chrome('chromedriver.exe')
    chrome_driver.maximize_window()
    chrome_driver.get('https://www.saucedemo.com/')
    actual_title = chrome_driver.title
    assert actual_title == 'Swag Labs'
    # chrome_driver.close()


def test_02():
    # Config file
    user_name = 'problem_user'
    password = 'secret_sauce'
    # Fixture
    chrome_driver = Chrome('chromedriver.exe')
    chrome_driver.maximize_window()
    chrome_driver.get('https://www.saucedemo.com/')

    # Page object
    user_input_locator = '//input[@id="user-name"]'
    user_input_element = chrome_driver.find_element(By.XPATH, user_input_locator)
    user_input_element.clear()
    user_input_element.send_keys(user_name)

    # Page object
    password_input_locator = '#password'
    password_input_element = chrome_driver.find_element(By.CSS_SELECTOR, password_input_locator)
    password_input_element.clear()
    password_input_element.send_keys(password)
    # password_input_element.send_keys(Keys.ENTER)

    # Page object
    login_button_locator = '//*[@type="submit"]'
    login_button_element = chrome_driver.find_element(By.XPATH, login_button_locator)
    login_button_element.click()
    time.sleep(2)
    navbar_button_locator = "//*[@id='react-burger-menu-btn']"
    navbar_button_element = chrome_driver.find_element(By.XPATH, navbar_button_locator)
    navbar_button_element.click()
    time.sleep(2)
    logout_button_locator = "//*[@id='logout_sidebar_link' and @class='bm-item menu-item']"
    logout_button_element = chrome_driver.find_element(By.XPATH, logout_button_locator)
    is_logout_button = logout_button_element.is_displayed()
    logout_button_element.click()
    time.sleep(2)
    assert is_logout_button is True, 'User was not logged-in'


# Safari - it didn't work, the browser wasn't opened :(

# def test_03():
#     safari_driver = Safari('/usr/bin/safaridriver')
#     safari_driver.maximize_window()
#     safari_driver.get('https://www.saucedemo.com/')
#     actual_title = safari_driver.title
#     assert actual_title == 'Swag Labs'
#
#
# def test_04():
#     # Config file
#     user_name = 'problem_user'
#     password = 'secret_sauce'
#     # Fixture
#     safari_driver = Safari('/usr/bin/safaridriver')
#     safari_driver.maximize_window()
#     safari_driver.get('https://www.saucedemo.com/')
#
#     # Page object
#     user_input_locator = '//input[@id="user-name"]'
#     user_input_element = safari_driver.find_element(By.XPATH, user_input_locator)
#     user_input_element.clear()
#     user_input_element.send_keys(user_name)
#
#     # Page object
#     password_input_locator = '#password'
#     password_input_element = safari_driver.find_element(By.CSS_SELECTOR, password_input_locator)
#     password_input_element.clear()
#     password_input_element.send_keys(password)
#     # password_input_element.send_keys(Keys.ENTER)
#
#     # Page object
#     login_button_locator = '//*[@type="submit"]'
#     login_button_element = safari_driver.find_element(By.XPATH, login_button_locator)
#     login_button_element.click()
#     time.sleep(2)
#     navbar_button_locator = "//*[@id='react-burger-menu-btn']"
#     navbar_button_element = safari_driver.find_element(By.XPATH, navbar_button_locator)
#     navbar_button_element.click()
#     time.sleep(2)
#     logout_button_locator = "//*[@id='logout_sidebar_link' and @class='bm-item menu-item']"
#     logout_button_element = safari_driver.find_element(By.XPATH, logout_button_locator)
#     is_logout_button = logout_button_element.is_displayed()
#     logout_button_element.click()
#     time.sleep(2)
#     assert is_logout_button is True, 'User was not logged-in'
#

