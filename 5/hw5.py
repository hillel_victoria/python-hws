# 1. Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

# Я знаю, що можна вирішити і без функцій, але довелось трохи забігти вперед і з функцією вийшло простіше
# Було б цікаво на уроці після дедлайну цього ДЗ розглянути це завдання, я писала декілька циклів всередині циклу і трохи в них заплуталась

print('Task 1')

user_str = input('Type some words: ')
vowels = 'aeoiuyAEOIYU'  # замість lower()
print(type(vowels))
word_count = 0
def my_func(word, vowels):
    not_vowels = ''
    for i in word:
        if i in vowels:
            not_vowels += i
        else:
            not_vowels += ' '
    for vow_letters in not_vowels.split():
        if len(vow_letters) >= 2:
            return word


if len(user_str) > 1:
    for word in user_str.split():
        if my_func(word, vowels):
            word_count += 1
    print('Number of words that have 2 vowels in a row: ', word_count)
else:
    print(f'{user_str} is too short, please try again')



# 2. Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166,
# "the_partner": 38.988, "sota": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

print('Task 2')
my_dict = {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324,
           "x-store": 37.166, "the_partner": 38.988, "sota": 37.720, "rozetka": 38.003}

lower_limit = 35.9
upper_limit = 37.339

for key, value in my_dict.items():
    if value  >= lower_limit and value <= upper_limit:
        print('Shop with prices in a range: ', key)

print('same but in one line:')
keys = [key for key, value in my_dict.items() if lower_limit <= value <= upper_limit]
print('Shops with prices in a range: ', keys)

