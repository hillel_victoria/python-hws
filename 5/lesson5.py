# # tuple - кортеж - is the same as list BUT can't be changed
#
# my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, 1) # can't change it
# print(my_tuple)
# print(type(my_tuple))
#
# print(my_tuple[0])
# print(my_tuple[3:6])
#
# my_tuple = (1,) # need to put a comm, so it will be tuple
# print(type(my_tuple))
#
# print(type(my_tuple[0]))
#
# my_tuple = (1, 2.0, True, 'four', [1, 2, 3], None, 1)
# print(type(my_tuple[4])) # can change a list itself
# my_tuple[4].append(4)
# print(my_tuple)
#
# my_tuple = (1, 2.0, True, 'four', (1, 2, 4), None, 1)  + (1,2,4) # will create a new tuple based on previous one; ((1,2,4),) - will add tuple
# print(my_tuple)
#
# my_tuple = 1, 2, 3, 4
# print(type(my_tuple))
#
# print('abcd' in my_tuple)
#
# my_tuple = (1, 2.0, True, 'four', [1, 2, 4], None, (1,2,4), 1)
# for elem in my_tuple:
#     print(elem, type(elem))
#
# print((1, 2, 3) == (1, 2, 4))
#
# for i in range(5):
#     print(i)

# for i in range(3,9):
#     print(i)

# my_list = list('qwertyuio') # convert string to list
# print(my_list)
#
# my_tuple = tuple('asdfgh')
# print(my_tuple)
#
# my_list = list(my_tuple)
# print(my_list)
#
# my_tuple = tuple(range(5))
# print(my_tuple)
#
# my_list = list(range(5, 15, 3)) # from x to y with step z
# print(my_list)
#
# my_list = list(range(15, 5, -2))
# print(my_list)

# set купа, куча

my_set = {1, 2.0, True, 'qwerty', None, (1,2,3,), 1, 2, 3}
# 1) only immutable elements, so no list;
# 2) only unique data
# 3) no indexes, so no slices
# 4) can add data
# 5) doesn't save order

# print(my_set)

# my_set.add(1) # 1 is already exist, so it will ignore that
# print(my_set)
# my_set.add('1')
# print(my_set)
#
# my_set.update({1, 2, 3, 4, 5, 6})
# print('updated: ', my_set)
#
# my_set.remove('1') # delete according to value
# print(my_set)
#
# print('1' in my_set)
#
# my_set.pop() # remove random element
# print(my_set)
#
# for elem in my_set:
#     print(elem) # can be random order
#     print(type(elem))
#
# my_set = set('qwertrewqwerew')
# print(my_set) # will take only unique values in random order
#
# my_set = set([4, 1, 2, 4, 1, 2, 4, 5, 3, 3]) # will take only unique values in random order
# print(my_set)

# frozen set - can't change it

# my_frset = frozenset([4, 5,1, 2, 4, 1, 2, 4, 5, 3, 3])
# print(my_frset)

# type dict -  словник, dictionary

# keys - unique, immutable, so no lists
# value - any type, everything

# my_dict = {'key_1': 1,
#            'key_2': 2,
#            1: 2,
#            2.2: 2,
#            False: 'qwerty',
#            None: None,
#            (1, 2, 3): [1, 2, 3, 4, 5]
# }

# print(my_dict)
# print(my_dict['key_1']) # access according to key []
# print(my_dict[(1, 2, 3)])
# print(my_dict[False])
#
# my_dict['key_1'] = 1000
# print(my_dict)
#
# my_dict[(1, 2, 3)][0] = '1234'
# print(my_dict)
#
# my_dict['qwert'] = 'ttt' # if key doesn't exist - it will create a new key
# print(my_dict)
#
# for i in my_dict:
#     print('--->', i, ' : ', my_dict[i])
#
# for i in my_dict.keys():
#     print('--->', i)
#
# for i in my_dict.values():
#     print('--->', i)

# for i in my_dict.items(): # return pair - key and value
#     print('--->', i)
#     print('key is ', i[0])
#     print('value is ', i[1])
#
#
# for i in my_dict.items():
#     if i[0] == 'key_2':
#         print('----------for', i[0])

# for key, value in my_dict.items():
#     print('key is ', i[0])
#     print('value is ', i[1])

my_dict = {'key_1': 1,
           'key_2': 2,
           1: 222222,
           2.2: 2,
           False: 'qwerty',
           None: None,
           (1, 2, 3): [1, 2, 3, 4, 5]
}

# print((my_dict[123])) # doesn't work
# print(123 in my_dict.keys())

# print(my_dict.get(1)) # we have key 1
# print(my_dict.get(123)) # we don't have key 123
# print(my_dict.get(123, 'asdfg')) # my_dict[123] if 123 in my_dict.keys() else 'asdfg'

# my_dict.update({'a': 'b', 'c': 'd', None: 'qwertqwer'}) # change dict using another dict, if key exists - it will replace it
# print(my_dict)

# my_dict |= {1: 2, 2: 3} # union; +=, + won't work
# print(my_dict)
#
# my_dict = {} # empty dict
# print(type(my_dict))
# my_dict[1] = 1
#
# my_dict.pop(1)
# print(my_dict)

# print(bool({})) # without any keys False
# print(bool({1: 'qwe'})) # 1+ key - True
#
# my_dict = dict([(1, 2), (3,4)])
# print(my_dict)
#
# my_dict = dict(a = 1, bb = 2, c = 3)
# print(my_dict)

# res = my_dict.get('key_3') # if no such key - will be None, it's just get, it doesn't change dict
# print(res)
#
# res = my_dict.get('key_3', True) # will use our value if key doesn't exist
# print(res)

# res = my_dict.setdefault('key_2') # if key exist - it will ignore it
# print(res)
# print(my_dict)
#
# res = my_dict.setdefault('key_3', 'default value') # if key doesn't exist, it will use default value
# print('--->', res)

# my_dict = {
#     'key_1': 'waa',
#     'key_2': 2
# }
#
# print(my_dict['key_1'],['1'])

lst = list(my_dict.keys())
print(lst)
lst = list(my_dict.values())
print(lst)
lst = list(my_dict.items())
print(lst)

