class Account:
    __total_money = 0


    def __init__(self, name, summ):
        self.__name = name
        self.__money = summ
        Account.accounting(summ, income=True)

    def __del__(self):
        Account.accounting(self.__money, income=False)
        self.__money -= self.__money
        print('Account was closed \U0001F4B0', self.__name)

    @classmethod
    def accounting(cls, summa: int, /, *, income: bool):
        if income:
            Account.__total_money += summa
        else:
            Account.__total_money -= summa


    def inc(self, m):
        self.__money+=m
        Account.accounting(m, income=True)



if __name__ == '__main__':
    dimon = Account('Дмитро', 1000)
    dimon2 = Account('Дмитро Юрійович', 100_000)
    dimon.money=500000000


    # del dimon2

    try:
        print(dimon2._Account__name)
    except:
        print('99999999999999999999999999999999999999')

    sam = Account('Sam', 100_000)
    print()
