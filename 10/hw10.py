import uuid
import datetime
from art import *


class Bank_account:
    __total_money = 0

    def __init__(self, user_name, money, percent, date_open: list[int], account_id):
        """
        Initialize self
        Args:
            user_name: name of the owner of the account
            money: initial amount of the contribution
            percent: percentage of accrual on the account balance (int)
            date_open: date when the account was opened
            account_id: id of the account
        """
        self.user_name = user_name
        self.__money = money
        self.__percent = percent
        self.date_open = datetime.datetime(*date_open)
        self.account_id = uuid.uuid4()


    @property
    def get_date_open(self):
        return self.date_open

    @property
    def account_rate_change(self):
        return self.__percent

    @account_rate_change.setter
    def account_rate_change(self, percent: int):
        self.__percent = percent

    @account_rate_change.getter
    def account_rate_change(self, percent: int):
            """Expected number is """

    def __str__(self):
            return f'{self.user_name} {self.account_id}'


    @classmethod
    def accounting(cls, summa: int, /, *, income: bool):
        if income:
            Bank_account.__total_money += summa
        else:
            Bank_account.__total_money -= summa

    def inc(self, m):
        self.__money += m
        Bank_account.accounting(m, income=True)

    def outcome(self, m):

        if self.__money >= m:
            self.__money -= m

    def money_transfer(self, other, summa):
        """Transferring the money"""
        if other.money >= summa:
            self.__money += summa
            other.money -= summa
        else:
            self.__money += other.money
            other.money = 0

    def __del__(self):
        Bank_account.accounting(self.__money, income=False)
        print(self.user_name, '\'s account was closed due to bank liquidation,', self.__money, 'was returned, the owner has no claims' )
        self.__money -= self.__money

    @property
    def get_todays_profit(self):
        return self.__money * self.__percent / 365 / 100


    @staticmethod
    def bank_slogan():
        print('This is your best bank', art("star"))



Peter = Bank_account('Peter', 1000, 5, [2022, 10, 10], 4567)

Peter.money = 500000000
Tom = Bank_account('Tom', 50000, 5, [2022, 11, 10], 4567)

Peter.inc(40000)
Peter.outcome(30000)

Tom.money_transfer(Peter, 5000)

Bank_account.date = [2022, 5, 10]


Peter.bank_slogan()
