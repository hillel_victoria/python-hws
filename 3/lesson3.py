# DRY - don't repeat yourself
# KISS - keep it simple stupid
# YAGNI - you ain't gonna need it

# booleans is and not

# loop

# while <cond>
#----4 spaces/tab

counter = 0

while counter < 3:
    print(f'Hello world - {counter}')
    counter += 1
print('end of loop')


# while True:    # this will be endless loop
#     print(f'Hello world - {counter}')
#     counter += 1
# print('end of loop')

while counter < 10:
    print(f'Hello world - {counter}')
    if counter % 2 == 0:
        print('Match odd')
    counter += 1
print('end of loop')

while True:
    print(f'Hellooo world - {counter}')
    if counter % 2 == 0:
        print('True - Match odd')
    counter += 1

    if counter > 10:
        break
print('end of loop')


while True:
    counter += 1
    if counter % 2 == 0:
        continue
    print(f'Hellooo world - {counter}')

    if counter > 10:
        break
print('end of loop')

# endless loop until user won't type digits

# while True:
#     user_input = input('Enter int, please: ')
#     if user_input.isdigit():
#         print(f'Thanks for {user_input}')
#         break
#     #else: - not must
#     print(f'{user_input} - it is not int!')

# str
# indexes starts fron 0

my_str = 'qwertayuia'

print(my_str.find('sd'))

#   .find()
#my_str = '0123456789'
print(my_str.find('345'))


#   access from zero

print(my_str.find('a')) # if there are a few 'a' - it will return the first one
print(my_str.find('a',3,7)) # look between 3 and 7 indexes


# to find symbol according to index []

print(my_str[0])
# print(my_str[20]) # error because 20th index is not exist there
print(len(my_str))
print(my_str[len(my_str)-1])
print(my_str[-1]) # to count from right to left (the last symbol)
print(my_str[-2])

#   slices
print('-------slices--------')

my_str = '0123456789'
print(my_str)
print('my_str[0]', my_str[0])
print('my_str[2:5]', my_str[2:5]) # show from 2nd to 5th (not including 5th); slices
print('my_str[3:15]', my_str[3:15]) # from 3rd to the end (cause we have less than 15)
print('my_str[-7:-3]', my_str[-7:-3]) # opposite - from right to left
print('my_str[-1:4]', my_str[-1:4]) # nothing, cause it started from the last one  with step 4
print('my_str[-1:4:-1]', my_str[-1:4:-1]) # it started from the last one  with step 4 with step -1
print('my_str[-0:9:3]', my_str[0:9:3]) # from 0 to 9, every 3 (step)
print('my_str[:5]', my_str[:5]) # until 5
print('my_str[4:]', my_str[4:]) # from 4
print('my_str[4::2]', my_str[4::2]) # from 4 with step 2
print('my_str[:8:2]', my_str[:8:2]) # before 8 with step 2
print('my_str[::-1]]', my_str[::-1]) # all with step -1

a = 1
b = 2
c = -1

print('my_str[a:b:c]',my_str[a:b:c])

print('my_str[len(my_str)//2:', my_str[len(my_str)//2:])

print('------------try except------------')

# try except - error catch

# try:
#----code; for example: 1/0
#----var1 = float (var2)
# except:
#----what to do if there is an error

# if there is no error - 'except' will be ignored

# try:
#     user_input = input('Enter int: ')
#     user_input = int(user_input) # or user_input = int(input('Enter int: '))
#
# except:
#     print('It\'s not an int')
# print('Done')

# user_input = input('Enter int: ')
# try:
#
#     user_input = int(user_input) # or user_input = int(input('Enter int: '))
#     user_input = 10 / user_input
#
# except ZeroDivisionError as e: # can be without 'as e' - it can be saved to the object e
#     print('It\'s a ZERO')
# except ValueError as e:
#     print('It\'s a ZERO')
#
# print('Done')


# user_input = input('Enter int: ')
# try:
#
#     user_input = int(user_input) # or user_input = int(input('Enter int: '))
#     user_input = 10 / user_input # if input == 0, then it's division by zero
#
# except Exception as e: # but better to specify the error, not just exception
#     print(f'It\'s a exception {type(e)}, {e}') # find what is the exception type - object e
# except:
#     print('Error') # if we don't know all the errors better to add this
#
# print('Done')

# user_input = input('Enter int: ')
# try:
#
#     user_input = int(user_input) # or user_input = int(input('Enter int: '))
#     user_input = 10 / user_input
#
# except (ZeroDivisionError, ValueError) as e: # can add a few types
#     print('error')
#
# print('Done')


# user_input = input('Enter int: ')
# try:
#
#     user_input = int(user_input) # or user_input = int(input('Enter int: '))
#     user_input = 10 / user_input
#
# except:
#     print('Error')
# else: # to know if there is or isn't error
#     print('Without error')
# print('Done')


# user_input = input('Enter int: ')
# try:
#
#     user_input = int(user_input) # or user_input = int(input('Enter int: '))
#     user_input = 10 / user_input
#
# except:
#     print('Error')
# else: # to know if there is or isn't error
#     print('Without error')
# finally: # finally is not a must
#     print('Final code')
# print('Done')

# loop with try

# while True:
#     user_input = input('Give me float! ')
#     # check data for float
#
#     try:
#         result = float(user_input)
#     except ValueError as e:
#         print(f'It\'s not a float: {user_input}')
#     else:
#         print('Thanks!')
#         break # here a must

# counter = 0
# while True:
#     counter += 1
#     while True:
#         user_input = input('Give me float! ')
#         # check data for float
#
#         try:
#             result = float(user_input)
#         except ValueError as e:
#             print(f'It\'s not a float: {user_input}')
#         else:
#             print('Thanks!')
#             break  # here a must
#     if counter >= 3:
#         break

# rarely used
counter = 0
while counter < 5:
    print(f'Inside WHILE {counter}')
    counter += 1
else:
    print(f'Inside ELSE {counter}')

