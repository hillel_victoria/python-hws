# 1. Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".



print('Task 1')
user_word = input('Enter the word: ')
while True:
    word_index = input('Enter symbol number: ')
    if word_index.isdigit():
        word_index = int(word_index)
        if word_index > len(user_word):
            print(f'The word \'{user_word}\' has less than {word_index} symbols')
        else:
            print(f'The {word_index} symbol in \'{user_word}\' is \'{user_word[word_index - 1]}\' ')
            break
    else:
        print(f'{word_index} is not a correct number')


# 2. Написати цикл, який буде вимагати від користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі). Цикл не повинен завершитися, якщо користувач ввів слово без букви о.

print('Task 2')
while True:
    user_input = input('Type the word with letter \'o\': ')
    if user_input.isalpha() and len(user_input) > 1:
            if 'o' not in str(user_input) and 'O' not in str(user_input): # Я пробувала "if 'o' and 'O' not in str(user_input)" але так не працює
                print(f'There is no \'o\' in {user_input}, please try again')
            else:
                print(f'Thanks! The word {user_input} has letter \'o\'')
                break
    else:
        print(f'{user_input} is not a word, please try again')
