import time

from selenium.webdriver import Chrome, Firefox, Safari, Keys
from selenium.webdriver.common.by import By


def test_01():
    driver_chrome = Chrome('chromedriver.exe')
    driver_chrome.maximize_window()
    driver_chrome.get('https://admin-demo.nopcommerce.com')

    # time.sleep(3)
    actual_title = driver_chrome.title
    expected_title = 'Your store. Login'
    assert actual_title == expected_title
    driver_chrome.quit()


def test_login():
    driver_chrome = Chrome('chromedriver.exe')
    driver_chrome.maximize_window()
    driver_chrome.get('https://admin-demo.nopcommerce.com')

    login = 'admin@yourstore.com'
    password = 'admin'


    # Enter login
    email_input_locator = '//input[@id="Email"]'
    time.sleep(3)
    email_input_element = driver_chrome.find_element(By.XPATH, email_input_locator)
    email_input_element.send_keys(Keys.BACKSPACE)
    email_input_element.send_keys(Keys.BACKSPACE)
    email_input_element.send_keys(Keys.BACKSPACE)
    email_input_element.send_keys(Keys.BACKSPACE)
    #email_input_element.clear()
    time.sleep(2)
    email_input_element.send_keys(login)

    # Enter password

    password_input_css_locator = '#Password'
    password_input_element = driver_chrome.find_element(By.CSS_SELECTOR, password_input_css_locator)
    password_input_element.clear()
    password_input_element.send_keys(password)

    # Login

    login_button_css_selector = 'button'
    login_button_element = driver_chrome.find_element(By.CSS_SELECTOR, login_button_css_selector)

    login_button_element.click()
    time.sleep(2)

    # Verify login

    logout_button_locator = "//div[@id='navbarText']//a[text()='Logout']"
    logout_button_element = driver_chrome.find_element(By.CSS_SELECTOR, logout_button_locator)
    is_logout_button = logout_button_element.is_displayed()
    driver_chrome.save_screenshot('test_screenshot.jpg')
    assert is_logout_button is True, f'User is not logged-in'
    driver_chrome.quit()