# pip install requests  - in terminal
import time
from pprint import pprint # pretty print
import requests

# pip freeze > requirements.txt - need to attach to HW if smth was installed
# pip list
# pip install -r requirements.txt

url = 'https://www.w3schools.com/python/'
response = requests.get(url)
# print(response.content)
# print(response.text)
# print(response.json()) # will fail

# url = 'https://dummyjson.com/products?limit=10' # dummy json responses
# url = 'https://dummyjson.com/products/{page}'
#
# for id in range (1, 11): # products from 1 to 10
#     response = requests.get(url.format(page=id))
#     pprint(response.json(), width=120)
#     # print(response.json())


url = 'https://dummyjson.com/products?limit=100'

response = requests.get(url)
response_json = response.json()

# products = 1
products = response_json['products']
# pprint(products)
#
# for product in products:
#     pprint(product)

total_cost = 0
# f = open('tt.txt', 'a')
# print(454, 46545, 4545., file = open())

# while True:
#
#     total_cost = 0
#     for product in products:
#         #pprint(product, width=200)
#         total_cost += product['price'] * product['stock']
#     print(total_cost)
#     # time.sleep(60 * 60) # 60 sec * 60 min; time.sleep(3600) - 3600 sec
#     time.sleep(5) # send request once in 5 sec

for product in products:
#    pprint(product, width = 200)
    total_cost += product['price'] * product['stock']
# print(total_cost)

# pprint(product, width=200)

total_price = 0
for product in products:
    if product['rating'] > 4.5:
        total_price += product['price'] * product['stock']

print(total_price)