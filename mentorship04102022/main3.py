import requests
from tabulate import tabulate
from pprint import pprint

url = 'https://dummyjson.com/comments?limit=340'

response = requests.get(url)
response_json = response.json()

n = ' awesome'.find('awesome') # n:1
# m = 'awesome'.count('awesome') # m:1
# k = 'awesome' in 'dsfsdf awesome'.split() # k:True
f = 'awesome' in 'dsfsdf awesome' # f:True


# comments = response_json['comments']
# for comment in comments:
#     if 'awesome' in comment['body'].lower():
#         print(comment['user']['username'])



# "comments":[
#   {
#       "id":1,
#       "body":"This is some awesome thinking!",
#       "postId":100,
#       "user": {
#           "id":63,
#           "username":"eburras1q"
#       }
#   }

a = ['aaa', 'bbb']
b = ['aaa', 'pppp']

set_a = set(a)
set_b = set(b)

print(set_a & set_b) # common
print(set_a.intersection(set_b))

print(set_a | set_b) # union
print(set_a.union(set_b))

print(set_a - set_b) # exist in a but not in b
print(set_a ^ set_b) # what is different in a and b

