import requests
from tabulate import tabulate
from pprint import pprint

url = 'https://dummyjson.com/todos?limit=150'

response = requests.get(url)
response_json = response.json()

# pprint(response_json['todos'], width=200)
todos = response_json['todos']
#print(tabulate(todos, headers='keys', tablefmt='grid')) # tablefmt='pipe'

for todo in todos:
    if todo['userId'] == 48 and todo['completed'] is False:
        print(todo['todo'])

