# functions

# -------------------------------- namespace - область видимості --------------------------------

# def my_function():
#     a = 10
#
#     print(a)
#     return a
#
# a = 100 # global namespace
# my_function()
# # print(a) - if 'a' from function - it won't work
# print(a)


# def my_function():
#     print('inside function', a) # won't type, cause 'a' is outside
#     return a
#
# print('outside function', a)
#
#
#
# def my_function(var):
#     print('inside function', var)
#     return a
#
#
# a = 100
# my_function(a)
#
# print('outside function', a)


# def my_function():
#     local_var = 100
#     print('inside function', local_var)

# def my_function():
#     local_var = 100
#     print('inside function', global_var)
#     #global_var += 1 # this won't work, we can't change global variables inside function
#
#
# global_var = 200
# my_function()
# print('outside function', global_var)
#
#
# def my_function(global_var):
#     global_var +=1
#     local_var = 100
#     print('inside function', global_var)
#
#
# global_var = 200
# my_function(global_var)
# print('outside function', global_var)


# def my_function(global_var):
#     print('inside function', global_var)
#     global_var.append(1) # don't do this
#
#
# global_var = []
# # my_function() # don't do like this if we are changing data inside function
# global_var = my_function(global_var) # this is better
# print('outside function', global_var)


# def foo():
#     pass
#
#
# def my_function(global_var):
#     print('inside function', global_var)
#     global_var.append(1)
#     foo()
#
#     return global_var
#
# global_var = []
# global_var = my_function(global_var)
# print('outside function', global_var)


# var_1 = 100
#
# def my_function():
#     print('inside function', var_1)
#     # var_1 += 1 # this won't work
#
#
# my_function()
#
# print('outside function', var_1)

# var_1 = 100
#
# def my_function():
#     global var_1 # if possible - don't use it
#
#     var_1 += 1
#     print('inside function', var_1)
#     # var_1 += 1 # this won't work
#
#
# print('outside function before', var_1)
# my_function()
#
#
# print('outside function after', var_1)


# -------------------------------- decorators --------------------------------


# a, b = 10, 20


# def foo (arg1, arg2):
#     print('inside function')

# we can use any type, if we didn't use decorators
# foo (1, 2.3)
# foo('1', True)
#
# if isinstance(a, int) and isinstance(b, int):
#     foo(a, b)


# def foo(arg1, arg2):
#     print('inside function', arg1, arg2)
#
#
# def my_decorator(func):
#
#     def wrapper(*args, **kwargs):
#         print('Some code before')
#         res = func(*args, **kwargs)
#         print('Some code after')
#
#         return res
#
#     return wrapper


# decorated = my_decorator(foo)
#
# decorated(1,2 )

#
# foo = my_decorator(foo)
# foo(1, 2)


# def my_decorator(func):
#
#     def wrapper(*args, **kwargs):
#         print('decorator Some code before')
#         res = func(*args, **kwargs)
#         print('decorator Some code after')
#
#         return res
#
#     return wrapper
#
# @my_decorator # foo = my_decorator(foo)
# def foo(arg1, arg2):
#     print('function inside function', arg1, arg2)
#
#
# foo(1, 2)


# def only_ints(func):
#
#     def wrapper(*args, **kwargs):
#         print('decorator only ints')
#         for arg in args:
#             if not isinstance(arg, int):
#                 raise TypeError(f'Wrong arg type {arg}')
#         for k, v in kwargs:
#             if not isinstance(v, int):
#                 raise TypeError(f'Wrong kwarg type {k} {type(v)}')
#
#         res = func(*args, **kwargs)
#
#
#         return res
#
#     return wrapper
#
#
# @only_ints # foo = only_ints(foo)
# def foo(arg1, arg2):
#     print('function inside function', arg1, arg2)
#
#
# # res = foo('juhgf', 2) # TypeError: Wrong arg type juhgf
# res = foo(5, 10)

# def type_checker(arg_type):
#     print('type_checker', arg_type)
#
#     def outer_decorator(func):
#
#         def inner_decorator(*args, **kwargs):
#             print('inner decorator')
#             for arg in args:
#                 if not isinstance(arg, int):
#                     raise TypeError(f'Wrong arg type {type(arg)}')
#             for k, v in kwargs:
#                 if not isinstance(v, int):
#                     raise TypeError(f'Wrong kwarg type {k} {type(v)}')
#
#             res = func(*args, **kwargs)
#             print('decorator inner_decorator')
#             return res
#
#         return inner_decorator
#
#     return outer_decorator
#
#
# def my_decorator(func):
#
#     def wrapper(*args, **kwargs):
#         print('decorator Some code before')
#         res = func(*args, **kwargs)
#         print('decorator Some code after')
#
#         return res
#
#     return wrapper
#
#
# @my_decorator
# @type_checker(str)
# def foo(arg1, arg2):
#     print('function inside function', arg1, arg2)
#
#
# res = foo('gfd', 'hgff')
# # res = foo(5, 10)


# -------------------------------- annotation опис --------------------------------

# def foo(arg1, arg2):
#     """
#     Doc string
#     Function for bla bla bla
#     Function for bla bla bla
#     Function for bla bla bla
#     """
#     print('function inside function', arg1, arg2)


# help(foo) # help

# -------------------------------- typing, type hints --------------------------------

# def foo(arg1, arg2):
#     """
#
#     :param arg1: description arg1
#     :type arg1: int
#     :param arg2: description arg2
#     :return: what is returned
#     """
#     # comment
#     print('function inside function', arg1, arg2)
#
#
# foo('1111', 2) # we will see hint here - that we need to use int


# def foo(arg1: int, arg2: float) -> str:   # it won't impact, but just specifying that we want to use types here and expected return is str
#     """
#     """
#     # comment
#     print('function inside function', arg1, arg2)
#     return str(arg1)
#
#
# # foo('1111', 2) # we will see hint here - that we need to use int
#
# res = foo('111', 2) # we will see hint here - that we need to use int


# def foo(arg1: int, arg2: float) -> str:
#     """
#     Bla bla bla
#     :param arg1: description arg1
#     :type arg1: int
#     :param arg2: description arg2
#     :return: what is returned
#     """
#     # comment
#     print('function inside function', arg1, arg2)
#     return str(arg1)
#
#
# # foo('1111', 2) # we will see hint here - that we need to use int
#
# res = foo('111', 2) # we will see hint here - that we need to use int
# help(foo)
#
# foo.__doc__
#
# foo.__annotations__


# -------------------------------- lambda однорядкова інструкція --------------------------------

# lst = [21, 2, 32, 1, 4, 5, 21, 35, 21, 4, 12, 54, 12, 56, 1, 0]
# res = sorted(lst)  # asc
# print(res)
#
# res = sorted(lst, reverse=True)  # desc
# print(res)
#
#
# def foo(arg):
#     return arg % 3
#
#
# lst = [21, 2, 32, 1, 4, 5, 28, 35, 213, 4, 12, 54, 12, 56, 1, 0]
# res = sorted(lst, key=foo)
# print(res)
#
# res = sorted(lst, key=lambda val: val % 3)  # = res = sorted(lst, key=foo)
# print(res)
# # lambda *args, **kwargs: return smth
#
# # bad use:
# foo = lambda val: val % 3  # BAD IDEA, better not to use and write function like in 327-328
#
# res = foo(2)
# print(res)
#
# # bad use
#
# foo = lambda val: int(val) % 3
# res = sorted(lst, key=lambda val: int(val))  # BAD IDEA
# res = sorted(lst, key=int)
#
# res = sorted(lst, key=lambda val: int(val) + 1)  # can exist...

# lambda arg1, arg2: arg1**arg2 + arg2**2


# ----------------------------------- type transform --------------------------

# int
# float
# bool
# str
# list
# tuple
# dict
# set
# frozenset

# math

# min / max

# lst = [21, 2, 32, 1, 4, 5, 28, 35, 213, 4, 12, 54, 12, 56, 1, 0]
# print(min(lst))
# print(max(lst))
#
# print(min(lst, key=lambda x: x % 3))
# print(max(lst, key=lambda x: x % 3))
#
# # sum
# print(sum(lst))
#
# # range
#
# res = range(1, 10 , 2) # from  x to y with step z
#
# for i in res:
#     print(i)

#
# enum = enumerate('Loren ipsum')  # create couples
#
# for i in enum:
#     print(i)


# def foo(val):
#     return val % 3
#
#
# lst = [21, 2, 32, 1, 4, 5]
# for i in lst:
#     i = foo(i)
#     print(i)
#
#
# # map, mapping
#
# mapped = map(foo, lst)
#
# for i in mapped:
#     print(i)
#
#
# for i in map(foo, lst): # same as above
#     print(i)
#
#
# for i in map(lambda x: x % 3, lst): # same as above
#     print(i)

# zip

# lst = [21, 2, 32, 1, 4, 5]
# my_str = 'asjd' # zip will cut to the shortest element
# my_set = {1, 4, 5, 7, 8, 9}
#
# # 21 a 1
# # 2 s 4
#
# zipped = zip(lst, my_str)
#
# for i in zipped:
#     print(i)
#
# zipped = zip(lst, my_str, my_set)
#
# for i in zipped:
#     print(i)
#
# for i in zip(lst, my_str):
#     print(i)

# reversed

# lst = [21, 2, 32, 1, 4, 5]
# rev = reversed(lst) # create new object, reversed
# print(rev)
#
# for i in rev:
#     print(i)

# filter
# lst = [21, 2, 32, 1, 4, 5]
#
# filtered = filter(lambda x: x % 2 == 0, lst)
#
# print(filtered)
# for i in filtered:
#     print(i)
#
#
# filtered = list(filter(lambda x: x % 2 == 0, lst))
# print(filtered)
#
# lst = [21, '2', 'dfgd', 32, 'defs', 1, 4, 5]
# filtered = list(filter(lambda x: isinstance(x, str), lst))
# print(filtered)


# any all

a, b = 10, 20

if a > b or b > 0 or b -a > 10:
    print('match')

conditions = (
    a > b,
    b > 0,
    b - a > 10
)

if any(conditions):
    print('match')

if all(conditions):
    print('all match')

