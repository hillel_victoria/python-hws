# Напишіть декоратор, який перетворює результат роботи функції на стрінг
# Напишіть докстрінг для цього декоратора

def change_to_str(func):
    """
    This decorator is a function that takes another function as its argument
    and changing the result of foo() function to string
    :return: wrapper arg
    """
    def wrapper(*args, **kwargs):

        print('Decorator: change to string')
        for arg in args:
            if not isinstance(arg, str):
                arg = str(arg)
                print(f'Type of {arg} was changed to {type(arg)}')
        for k, v in kwargs:
            if not isinstance(v, str):
                v = str(v)

        res = func(*args, **kwargs)

        return res

    return wrapper


@change_to_str
def foo(arg1, arg2):
    print('Arguments are:', arg1, arg2)


res = foo(44, True)

# res = foo('hhh', None)
