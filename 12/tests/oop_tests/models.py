from abc import ABC, abstractmethod
from typing import Union
from random import randint, choice
from faker import Faker
# from rich import * # not good to write with *

fake = Faker(locale='uk_UA')


class SchoolStaff(ABC):
    __slots__ = ('name', 'salary')

    def __init__(self, name: str, salary : Union[int, float]):
        self.name = name
        self.salary = salary

    @abstractmethod
    def __str__(self):
        pass


class Teacher(SchoolStaff):

    def __str__(self):
        return f'Я вчитель {self.name}, {self.salary}'


class TechnicalStaff(SchoolStaff):
    def __str__(self):
        return f'Я прибиральник {self.name}, {self.salary}'


# t = Teacher('Ivan', 45)
# t2 = TechnicalStaff('Vova', 55)

class School:
    def __init__(self, name: str, principal: Teacher, number_of_teachers: int = 10, number_of_tech_staff: int = 5):
        self.name = name
        self.principal = principal
        self.teachers = [Teacher(fake.name(), randint(5000, 30000)) for position in range (number_of_teachers)]
        self.tech_staff = [TechnicalStaff(fake.name(), randint(1000, 4500)) for position in range (number_of_tech_staff)]

    @property
    def get_total_salary(self):
        all_staff = []
        all_staff.append(self.principal)
        all_staff += self.teachers
        all_staff += self.tech_staff
        total = sum((obj.salary for obj in all_staff))
        return total

    def set_new_principal(self):
        #print(self.principal)
        old_principal = self.principal
        new_candidate = self.teachers.pop()
        #print(len(self.teachers))
        #print(new_candidate)
        self.principal = new_candidate
        self.teachers.append(old_principal)


school5 = School('School 5', Teacher('Вікторія', 25000))
school5.teachers.append(Teacher('Володимир', 100_000))
school5.set_new_principal()
print()