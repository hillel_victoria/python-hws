# functions

# print()
# input()
# int()


# syntax

# while True:
#     try:
#         age = int(input('Age '))
#     except:
#         print('Wrong data')
#     else:
#         break

def get_int_number(message):
    while True: # function body
        try:
            age = int(input(message))
        except:
            print('Wrong data')
        else:
            break

    return age # return will stop the function, we can use it as 'break'


user_age = get_int_number('Give me your age: ')
print(user_age)

# length = get_int_number('Length? ')
# print(length)

# def my_function(arg1, arg2):
#     result = arg1 + arg2
#
#     return result
#
#
# res = my_function(1, 2) # our function has 2 arguments, so we need to put 2 numbers
# print(res)
#
# res = my_function(2, 10)
# print(res)

# def my_function(arg1, arg2):
#     result = arg1 + arg2
#
#     # return result # return is used when we need to return data
#
#
# res = my_function(1, 2)
# res2 = print(res)


# def my_func():
#     for i in range(10):
#         print('--->', i)
#
# my_func()

# result = 0 # this variable is not the same as the one inside function


# def my_function(arg1, arg2):
#     print('arg1', arg1)
#     print('arg2', arg2)
#     result = arg1 + arg2
#
#     return result
#
#
# res = my_function(2, 3) # позиційна передача аргументів
#
# def my_function(arg1, arg2, arg3):
#     print('arg1', arg1)
#     print('arg2', arg2)
#     result = arg1 + arg2 * arg3
#
#     # return result
#
# res = my_function(1, 2, 3) # позиційна передача аргументів
#
# res = my_function(arg1=1, arg2=2, arg3=3) # іменована передача аргументів - найкраще її використовувати
# var1 = 10
# var2 = 20
# var3 = 30
# res = my_function(arg1=var1, arg2=var2, arg3=var3)
#
# res = my_function(1, arg3=2, arg2=3) # комбінована передача аргументів; спочатку позиційні, потім іменовані
#
# res = my_function(1, 2, arg3=3)


# def my_function(arg1, arg2, arg3):
#
#     res = arg1 + arg2 + arg3
#
#     return res
#
#
# result = my_function('1234', '12345', 'dfdsf')

# необов'язкові аргументи:
# default values

# def my_function(arg1, arg2, arg3=0): # can't do this: my_function(arg1=0, arg2, arg3), the dafault values should be in the end
#     print('arg1', arg1)
#     print('arg2', arg2)
#     print('arg3', arg3)
#
#     res = arg1 + arg2 + arg3
#
#     return res
#
#
# res = my_function(1, 2, 3)
# res = my_function(1, 2) # 3 is not needed here, because arg3=0
#
#
# # def my_function(arg1=0, arg2=0, arg3=0) - this is fine

# def my_function(arg1, arg2, arg3):
#
#     print('arg1', arg1)
#     print('arg2', arg2)
#     print('arg3', arg3)
#
#     return
#
#
# res = my_function('1', [2], {1:2})
#
#
# def my_function1(lst1):
#
#     print('lst1', lst1)
#     lst1.append(1) # not very good practice to change mutable objects, but fine if using return lst1
#     return lst1
#
#
# my_list = [1, 2, 3]
# print('my_list', my_list)
# my_list = my_function1(my_list)
# print('lst1', my_list)


# def my_function1(lst1=[]): # not recommended to put mutable object as default value
#
#     print('lst1', lst1)
#     lst1.append(1)
#     return lst1


# my_list = [1, 2, 3]
# print('my_list', my_list)
# my_list = my_function1(my_list)
# print('my_list', my_list)
# my_list2 = my_function1()
# print('my_list2', my_list2)
#
# my_list2 = my_function1()
# my_list2 = my_function1()
# my_list2 = my_function1() # every time it's adding element - happening when using default value

# print('my_list2', my_list2)

# def my_function1(lst1=None): # this function is better
#     print('lst1', lst1)
#     if lst1 is None:
#         lst1 = []
#     lst1.append(1)
#     return lst1
#
# my_list2 = my_function1()
# my_list2 = my_function1()
# my_list2 = my_function1()
# my_list2 = my_function1()
# my_list2 = my_function1() # doesn't matter how many times, it still will have 1 element
# print(my_list2)

'''
from homework 4:

Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
Зауважте, що lst1 не є статичним і може формуватися динамічно.
'''


# def my_function(lst):
#     result_list = []
#
#     if not isinstance(lst, (list, tuple)): # isinstance is similar to: type(lst) == list; can check a few types
#         return result_list
#
#     for i in lst:
#         if isinstance(i, str):
#             result_list.append(i)
#     return result_list
#
#
# lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
# res = my_function(lst1)
# print(res)
#
# res = my_function(123456) # will return error, cause function should return one data type
# print(res)



# def my_function(a, b, c, *args): # *args - невідома кількість аргументів; args - just a name, but standard
#     print('Inside function')
#     print(a, b, c)
#     print(type(args))
#     print(args)
#
#
# my_function(1, 2, 3)
# my_function(1, 2, 3, 4, 5, 6, 7, 8, 9, 0) # because of *args we can use many arguments, not only 3

# def my_function(*args):
#     print('Inside function')
#     print(type(args))
#     print(args)
#
#     for i in args:
#         print(i)
#
#
# my_function(1)
# my_function(1, 2, 3, 4)
# my_function(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)

# def my_function(arg1, arg2=0, *args): # correct order
#     print('Inside function')
#     print(type(args))
#     print(args)
#
#     for i in args:
#         print(i)
#
#
# my_function(1, 2)

# def my_function(arg1, arg2=0, **kwargs): # kwargs - keyboard args **
#     print('Inside function')
#     print(type(kwargs))
#     print(kwargs)
#
#     for i in kwargs:
#         print(i)
#
#
# my_function(1, 2, a=10, b=20, c=10) # we need 1-2 position arguments

#
# my_function(1, 2)

# def my_function(arg1, arg2=0, *args, **kwargs): # we can put there everything
#     print('Inside function')
#     print(args)
#     print(kwargs)
#
#
# my_function(1, 2, 3, 4, 5)
# my_function(1, 2, 3, 4, 5, a=10, b=20)


# def my_function(arg1, arg2, arg3, arg4):
#     print('Inside function')
#     print(arg1, arg2, arg3, arg4)
#
# lst = [1, 2, 3, 4]
#
# my_function(*lst) # передати функцію позиційно  = my_function(1, 2, 3, 4)


# def my_function(**kwargs):
#     print('Inside function')
#     print(kwargs)
#
# dct = {'a': 10, 'b': 20, 'c': 30}
#
# my_function(**dct) # = my_function(a=10, b=20)
#
# def my_function(a, b, **kwargs):
#     print('Inside function')
#     print(kwargs)
#
#
# my_function(**dct)




# lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
# lst2 = []
#
# # regular for
# for i in lst1:
#     if isinstance(i, str):
#         lst2.append(i)

# comprehension

# we can use comprehension inside comprehension, but not recommended

# lst2 = [i for i in lst1 if isinstance(i, str)]
#
# iterable = 'qwerty'
# lst3 = [i * 2 for i in iterable]
#
# print(lst3) # ['qq', 'ww', 'ee', 'rr', 'tt', 'yy']
#
# lst4 = [i * 2 for i in iterable if isinstance(i, str)]
#
# print(lst4) # ['qq', 'ww', 'ee', 'rr', 'tt', 'yy']
#
# lst5 = [i ** 2 for i in range(101) if i % 2 == 0] # ** square
# print(lst5)
#
# my_set = {i ** 2 for i in range(101) if i % 2 == 0}
# print(my_set)
#
# my_tuple = (i ** 2 for i in range(1100000) if i % 2 == 0) # type generator, not my_tuple
# print(type(my_tuple))
# print(my_tuple)


