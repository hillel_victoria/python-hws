'''
Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:

Попросіть користувача ввести свсвій вік.

- якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
- якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
- якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
- якщо вік користувача містить 7 - вивести "Вам <>, вам пощастить"
- у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"

Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік

Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...).
Наприклад :

"Тобі ж 5 років! Де твої батьки?"
"Вам 81 рік? Покажіть пенсійне посвідчення!"
"Незважаючи на те, що вам 42 роки, білетів всеодно нема!"

Зробіть все за допомогою функцій! Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду.
'''


def input_age():
    user_age = input('Введіть ваш вік:')
    return user_age


user_age = input_age()
# print(user_age)


def last_age_number():

    age1 = 'рік'
    age2 = 'роки'
    age3 = 'років'
    # age_end = ''

    if user_age.endswith('1') and len(user_age) == 1:
        age_end = age1
    elif user_age.endswith('1') and user_age.startswith(('2', '3', '4', '5', '6', '7', '8', '9', '10', '12')):
        age_end = age1
    elif user_age.endswith(('2', '3', '4')) and user_age.startswith(('2', '3', '4', '5', '6', '7', '8', '9', '10')):
        age_end = age2
    elif user_age.endswith(('5', '6', '7', '8', '9', '0', '11', '12', '13', '14', '15', '16', '17', '18', '19')):
        age_end = age3
    else:
        age_end = '' # щоб уникнути помилки коли юзер вводить 121, 132, 142 і тд
    return age_end


age_end = last_age_number()
# print(age_end)


def print_func():

    if user_age.isdigit() and len(user_age) <= 3:
        userageint = int(user_age)
        if '7' in str(userageint) and userageint < 120:
            print(f'Вам {userageint} {age_end}, вам пощастить')
        else:
            if userageint < 7:
                print(f'Тобі ж {userageint} {age_end}! Де твої батьки?')
            elif userageint < 16:
                print(f'Тобі лише {userageint} {age_end}, а це це фільм для дорослих!')
            elif userageint > 120:
                print(f'Вам {userageint} {age_end}? Ви що зомбі? Введіть справжній вік!')
            elif userageint > 65:
                print(f'Вам {userageint} {age_end}? Покажіть пенсійне посвідчення!')
            else:
                print(f'Незважаючи на те, що вам {userageint} {age_end}, білетів все одно нема!')
    else:
        print('Це не вік')


text_value = print_func()
# print(text_value)


