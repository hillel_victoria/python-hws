# напишіть функцію, яка перевірить, чи передане їй число є парним чи непарним (повертає True False)
import types

print('Task 1')


def even_or_odd(number):
    if number % 2 == 0:
        return True
    else:
        return False


# even_or_odd(10)
number_is_even = even_or_odd(11)
print('Is the number even? ', number_is_even)

some_number = 9
assert even_or_odd(13) is False
# assert even_or_odd(some_number) <= 6, 'Should be True or False'
# assert even_or_odd(some_number) is None, 'Should be True or False'
assert even_or_odd(some_number + 1) is True
assert type(even_or_odd(122)) == bool

# напишіть функцію. яка приймає стрічку, і визначає, чи дана стрічка відповідає результатам роботи методу capitalize()
# (перший символ є верхнім регістром, а решта – нижнім.) (повертає True False)
#
# написати до кожної функції мінімум 5 assert

print('Task 2')


def cap_letters(string: str):
    string = str(string)
    if string.capitalize() == string:
        return True
    else:
        return False


# cap_result = cap_letters('Some text')
cap_result = cap_letters('HeLLo there')
# cap_result = cap_letters('hello')
# cap_result = cap_letters('66')
print('Does the text respond to capitalize() method? ', cap_result)

assert type(cap_letters('hi')) == bool
assert cap_letters('Hello') is True
# assert cap_letters('qwe') is None, 'Should be True or False'
assert cap_letters('text') is False
assert cap_letters('He' + 'l' * 2) is True
assert cap_letters(False) is True  # cause str(False) means 'False'
assert cap_letters(123) is True  # cause '123'

# написати декоратор, який добавляє принт з результатом роботи отриманої функції + текстовий параметр, отриманий ним
# (декоратор з параметром - це там, де три функції)
# при цьому очікувані результати роботи функції не змінюються (декоратор просто добавляє принт)

print('Task 3')


def decorator_with_args(text: str):
    print(text)

    def real_decorator(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            print('Result =', result)

            return result

        return wrapper

    return real_decorator


@decorator_with_args('Let\'s add \'a\' and \'b\':')
def args_sum(a, b):
    return a + b


args_sum(10, 25)

c = 5
d = 15

print('Results from assert:')  # Не знаю чому вони відображаються, коли функція викликається всередині assert
# assert type(args_sum(15.8, 20)) == int, 'Type must be int'
assert type(args_sum(33, 20)) == int
assert args_sum(10, 14) <= 100
assert args_sum(c - 5, d - 15) == 0
assert args_sum(d - c, d) > 0
assert args_sum(4, args_sum(5, 10)) > 2
assert type(decorator_with_args('some text to check type')) == types.FunctionType
