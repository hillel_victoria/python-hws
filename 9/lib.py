from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders

import os
import smtplib




def mail_sender(
        recipient: list,
        data_to_send: str,
        subject: str = 'Subject',
        attachment: str = None,
):
    """
    sending e-mail
    """

    SERVER = 'smtp.ukr.net'
    PASSWORD = '08T9SkVyOLd2rRWv'
    USER = 'test_hillel_api_mailing@ukr.net'

    recipient = [*recipient]
    sender = USER
    subject = subject
    text = data_to_send

    if attachment:
        filepath = attachment

        from os.path import exists
        file_exists = exists(filepath)
        if not file_exists:
            print('file unavailable')
            return False
        basename = os.path.basename(filepath)
        filesize = os.path.getsize(filepath)

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = 'Python script <' + sender + '>'
    msg['To'] = ', '.join(recipient)
    msg['Reply-To'] = sender
    msg['Return-Path'] = sender
    msg['X-Mailer'] = 'decorator'

    part_text = MIMEText(text, 'plain')

    if attachment:
        part_file = MIMEBase('application', 'octet-stream; name="{}"'.format(basename))
        part_file.set_payload(open(filepath, "rb").read())
        part_file.add_header('Content-Description', basename)
        part_file.add_header('Content-Disposition', 'attachment; filename="{}"; size={}'.format(basename, filesize))
        encoders.encode_base64(part_file)
        msg.attach(part_file)

    msg.attach(part_text)

    mail = smtplib.SMTP_SSL(SERVER)
    mail.login(USER, PASSWORD)
    mail.sendmail(sender, recipient, msg.as_string())
    mail.quit()
    return True


# mail_sender(
#     [
#         'test_hillel_api_mailing@ukr.net',
#         'test_hillel_api_mailing@ukr.net',
#     ],
#     'text'*999,
#     '*'*10000,
#     'lib.py'
# )