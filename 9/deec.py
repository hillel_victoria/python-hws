import lib
import datetime

API = 'kjdfvbhjdfjkghkdsfh'


def decorator(r):
    def dec(func):
        def wrapper(*args, **kwargs):
            user = 'bond'
            result = str(func(*args, **kwargs))
            if kwargs.get('login') == user:
                lib.mail_sender([r], result, str(datetime.datetime.now()))
            else:
                lib.mail_sender([r], 'wrong user', str(datetime.datetime.now()))

            return result
        return wrapper
    return dec


def dec_no_choice(func):
    def wrapper(*args, **kwargs):
        new_arg = []
        for arg in args:
            new_arg.append(str(arg))

        user = 'bond'
        result = func(*new_arg, login='kjhghjhgh')
        if kwargs.get('login') == user:
            lib.mail_sender(['test_hillel_api_mailing@ukr.net'], str(result[0]), str(datetime.datetime.now()))
        else:
            lib.mail_sender(['test_hillel_api_mailing@ukr.net'], 'wrong user', str(datetime.datetime.now()))

        return result
    return wrapper






@decorator('test_hillel_api_mailing@ukr.net')
def foo(par, login):
    """

    Args:
        par (str|int|float|list):
        login (str):

    Returns:

    """
    return par*2, login



@dec_no_choice
def foo1(par, login):
    """

    Args:
        par (str|int|float|list):
        login (str):

    Returns:

    """
    return par*2, login


# # foo = dec(foo)
# print(foo(500, login='bond'))
# # print(foo(5, login='bo6nd'))
# #
# #
# # print(foo1(5, login='bond'))
# print(foo1(5000, login='bond'))


def get_data():
    return 5


def checker(data):
    if data < 7:
        return True
    return False


def checker_array(data):
    if data < 7:
        return True
    return False


def main():
    data = get_data()
    age_spell = checker(data)
    b = checker_array(data)
    return age_spell, b


if __name__ == '__main__':
    main()

    if 5 > 6:
        print()
    else:
        print(5656)











# my_funcs = {
#     1: foo,
#     2: foo1,
# }
#
#
# my_funcs[1]('jhhgnhfhjhj', login='bond')
