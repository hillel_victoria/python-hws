# написати функцію (та покрити її тестами), яка приймає стрічку, а також інтовий параметр максимально допустимої довжини
# (за замовчуванням 70)
# якщо довжина отриманої стрічки менша чи дорівнює отриманому параметру, то функція повертає отриману стрічку без змін
# якщо ж отримана стрічка буде довшою за отриманий параметр, то вона обрізається до
# максимальний параметр мінус 3 символа (загальна довжина стрічки не має бути більшою за вказаний параметр)
# та до неї додається стрічка з трьох крапок ("...")
# та повертається ця обрізана стрічка


# def maleware_sms():


def string_cutter(string, max_allowed_lenght=70):
    """
    Function cuts the given string
    Args:
        string (str):
        max_allowed_lenght (int): it matters because of https://turbosms.ua/price.html

    Returns:
        (str):
    """

    if len(string) <= max_allowed_lenght:
        return string
    else:
        result = string[:max_allowed_lenght -3] + '...'
        return result


print(len(string_cutter('*' * 200, 80)))

assert type(string_cutter('')) == str
assert type(string_cutter('*' * 200)) == str
assert len(string_cutter('*' * 200, 80)) == 80
assert len(string_cutter('*' * 50, 80)) <= 80
assert string_cutter('*' * 500, 80)[-3:] == '...' # cutting from the end of string
assert string_cutter('*' * 50, 80)[-3:] == '***'

string = '0123456789'
assert string_cutter(string, 10)[-3:] == string[-3:]
assert string_cutter('+' * 10) == '+' * 10
assert string_cutter('+' * 550) != '+' * 500

# if 5 != 5:
#     raise ValueError

# assert 5 == 5
# assert 5 != 5, 'Custom message'

# assert func() is None


