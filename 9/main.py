# написати функцію (та покрити її тестами), яка приймає стрічку, а також інтовий параметр максимально допустимої довжини
# ( за замовчуванням 70)
# якщо довжина отриманої стрічки менша чи дорівнює отриманому параметру, то функція повертає отриману стрічку без змін
# якщо ж отримана стрічка буде довшою за отроиманий параметр, то вона обрізається до
# максимальний параметр мінус 3 символа (загальна довжина стрічки не має бути більшою за вказаний параметр)
# та до неї добавляється стрічка з трьох крапок ("...") та повертається ця "обрізана стрічка



def string_cutter(string, max_allowed_length=70):
    """
    Function cuts the given string
    Args:
        string (str):
        max_allowed_length (int): it matters because of https://turbosms.ua/price.html
    Returns:
        (str):
    """
    if type(string) != str:
        string = str(string)

    if len(string) <= max_allowed_length:
        return string
    else:
        result = string[:max_allowed_length - 3] + '...'
        return result


assert type(string_cutter(5)) == str
assert type(string_cutter('*' * 200)) == str
assert len(string_cutter('*' * 200, 80)) == 80
assert len(string_cutter('*' * 50, 80)) <= 80
assert string_cutter('*' * 500, 80)[-3:] == '...'

string = '0123456789'
assert string_cutter(string, 10)[-3:] == string[-3:]
assert string_cutter('+' * 500) != '+' * 500





